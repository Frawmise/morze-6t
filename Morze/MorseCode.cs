﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morze
{
    public class MorseCode
    {
        public char Betű { get; set; }
        public string Morzejel { get; set; }

        public MorseCode()
        {

        }
    }
}

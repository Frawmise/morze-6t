﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morze
{
    class Program
    {
        static List<MorseCode> morzeABC;
        static List<Quote> quotesInMorse;
        static List<Quote> quotes;
        static void Main(string[] args)
        {
            MasodikFeladat();

            HarmadikFeladat();

            NegyedikFeladat();

            OtodikFeladat();

            HetedikFeladat();

            DecodeAllTheQuotes();

            NyolcadikFeladat();

            KilencedikFeladat();

            TizedikFeladat();

            Console.WriteLine();
            Console.WriteLine("Nyomjon meg egy gombot a kilépéshez...");
            Console.ReadKey();
        }

        private static void TizedikFeladat()
        {
            if (!Directory.Exists(@"Output"))
            {
                Directory.CreateDirectory(@"Output");
            }

            using (var f = new StreamWriter(@"Output\forditas.txt", false, Encoding.Default))
            {
                foreach (var item in quotes)
                {
                    f.WriteLine(item.Szerző + item.Idézet);
                }
            }
        }

        private static void KilencedikFeladat()
        {
            Console.WriteLine("9. feladat: Arisztotelész idézetei:");

            foreach (var quote in quotes)
            {
                if (quote.Szerző.Trim() == "ARISZTOTELÉSZ")
                {
                    Console.WriteLine($"\t - {quote.Idézet}");
                }
            }
        }

        private static void NyolcadikFeladat()
        {
            Console.Write("8. feladat: ");

            var longestQuote = quotes.FirstOrDefault(lq => lq.Idézet.Length.Equals(quotes.Max(q => q.Idézet.Length)));

            Console.WriteLine($"A leghosszabb idézet szerzője és az idézet: {longestQuote.Szerző}: {longestQuote.Idézet}");
        }

        private static void DecodeAllTheQuotes()
        {
            quotes = new List<Quote>();

            foreach (var quote in quotesInMorse)
            {
                var tempQuote = new Quote
                {
                    Szerző = Morze2Szöveg(quote.Szerző),
                    Idézet = Morze2Szöveg(quote.Idézet)
                };

                quotes.Add(tempQuote);
            }
        }

        private static void HetedikFeladat()
        {
            Console.Write("7. feladat: ");
            Console.WriteLine($"Az első idézet szerzője: {Morze2Szöveg(quotesInMorse.First().Szerző)}");
        }

        private static string Morze2Szöveg(string textInMorse) //6. feladat
        {
            var result = "";

            var morseToDecode = new List<List<string>>();
            var wordToDecode = new List<string>();

            var spaceCount = 0;
            var temporaryMorseCode = "";

            for (int i = 0; i < textInMorse.Length; i++)
            {
                if (textInMorse[i] == ' ')
                {
                    if (temporaryMorseCode.Length > 0)
                    {
                        wordToDecode.Add(temporaryMorseCode);
                        temporaryMorseCode = "";
                    }
                    spaceCount++;
                }
                else
                {
                    if (spaceCount == 3 || spaceCount == 0)
                    {
                        temporaryMorseCode += textInMorse[i];
                        spaceCount = 0;
                    }
                    else if (spaceCount == 7 || spaceCount == 0)
                    {
                        if (temporaryMorseCode.Length > 0)
                        {
                            wordToDecode.Add(temporaryMorseCode);
                        }

                        morseToDecode.Add(wordToDecode);

                        wordToDecode = new List<string>();
                        temporaryMorseCode = "";
                        spaceCount = 0;

                        temporaryMorseCode += textInMorse[i];
                    }
                }
            }


            if (morseToDecode.Count == 0)
            {
                morseToDecode.Add(wordToDecode);
            }

            foreach (var word in morseToDecode)
            {
                foreach (var letter in word)
                {
                    foreach (var morseCode in morzeABC)
                    {
                        if (morseCode.Morzejel == letter)
                        {
                            result += morseCode.Betű;
                            break;
                        }
                    }
                }
                result += " ";

            }

            return result;
        }

        private static void OtodikFeladat()
        {
            quotesInMorse = new List<Quote>();

            using (var f = new StreamReader(@"txt files\morze.txt"))
            {
                while (!f.EndOfStream)
                {
                    var row = f.ReadLine().Split(';');

                    var temporaryQuoteInMorse = new Quote
                    {
                        Szerző = row[0],
                        Idézet = row[1]
                    };

                    quotesInMorse.Add(temporaryQuoteInMorse);
                }
            }
        }

        private static void NegyedikFeladat()
        {
            Console.Write("4. feladat: ");
            Console.Write("Kérek egy karaktert: ");
            var characterFromUser = Console.ReadLine();


            foreach (var item in morzeABC)
            {
                if (item.Betű.ToString() == characterFromUser)
                {
                    Console.WriteLine($"\t A {characterFromUser} karakter morze kódja: {item.Morzejel}");
                    return;
                }
            }
            Console.WriteLine("\t Nem található ilyen karakter a kódtárban.");
        }

        private static void HarmadikFeladat()
        {
            Console.Write("3. feladat: ");
            Console.WriteLine($"A morze abc {morzeABC.Count} db karakter kódját tartalmazza.");
        }

        private static void MasodikFeladat()
        {
            morzeABC = new List<MorseCode>();

            using (var f = new StreamReader(@"txt files\morzeabc.txt", Encoding.Default))
            {
                var headline = f.ReadLine();

                while (!f.EndOfStream)
                {
                    var row = f.ReadLine().Split('\t');

                    var temporaryMorseCode = new MorseCode
                    {
                        Betű = row[0].ToCharArray()[0],
                        Morzejel = row[1]
                    };

                    morzeABC.Add(temporaryMorseCode);
                }
            }
        }
    }
}
